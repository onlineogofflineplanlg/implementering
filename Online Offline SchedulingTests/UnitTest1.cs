﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online_Offline_Scheduling;
using Online_Offline_Scheduling.Models;
using OOS;

namespace Online_Offline_SchedulingTests
{
    [TestClass]
    public class Occupied
    {
        private Seat s;
        [TestInitialize]
        public void StartUp()
        {
            s = new Seat(1);
        }


        [TestMethod]
        public void Test4To5Passenger()
        {
            s.Passengers.Add(new Passenger(4, 5));
            Assert.IsFalse(s.Occupied(2, 4));
        }

        [TestMethod]
        public void Test2Times2To5Passenger()
        {
            s.Passengers.Add(new Passenger(4, 5));
            s.Passengers.Add(new Passenger(2, 4));
            Assert.IsTrue(s.Occupied(2, 4));
        }

        [TestMethod]
        public void Test0To2And4To6And2To4()
        {
            s.Passengers.Add(new Passenger(0, 2));
            s.Passengers.Add(new Passenger(4, 6));
            Assert.IsFalse(s.Occupied(2, 4));
        }

        [TestMethod]
        public void Test()
        {
            s.Passengers.Add(new Passenger(0, 1));
            s.Passengers.Add(new Passenger(1, 3));
            Assert.IsTrue(s.Occupied(1, 2));
        }
    }
}
