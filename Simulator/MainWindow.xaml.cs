﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Online_Offline_Scheduling;
using Online_Offline_Scheduling.Logic;
using Online_Offline_Scheduling.Logic.Sorting;
using Online_Offline_Scheduling.Models;

namespace Simulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Accommodating ac = new Accommodating(1000);
            ac.PlacePassengers();
            List<Passenger> testData = ac.ExtractPassengers(true);

            SortPassengers sp = new FirstFit(testData, 50);
            //Train t = new Train(50, 0, 15) {Seats = sp.Sort()};

            //Train currentTrain = t;
            //List<Passenger> passengers = Generator.GetPassengers(25, 0, size).OrderBy(x => x.Stop).ThenBy(x => x.Stop).ToList();

            //int passengerCount = seatList.Sum(x => x.Passengers.Count);
            //t.AssignSeats(passengers, Train.FittingAlgorithm.Best);


            /*foreach (var sg in currentTrain.Seats.Select(seat => new Seat(seat, currentTrain.Stop)))
                seatListseats.Children.Add(sg);
            Label stop = new Label
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                FontSize = 13.333,
                Content = "Stop"
            };
            stopLabels.Children.Add(stop);
            for (int i = 0; i < currentTrain.Stop+1; i++)
            {
                stopLabels.ColumnDefinitions.Add(new ColumnDefinition());
            }
            Grid r = new Grid
            {
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xE0, 0xE0, 0xE0)),
                Height = 3
            };
            r.SetValue(Grid.ColumnProperty, 1);
            r.SetValue(Grid.ColumnSpanProperty, currentTrain.Stop);
            stopLabels.Children.Add(r);
            for (int i = 0; i < currentTrain.Stop; i++)
            {
                Label station = new Label
                {
                    //HorizontalAlignment = HorizontalAlignment.Center,
                    //VerticalAlignment = VerticalAlignment.Center,
                    FontSize = 20,
                    Content = i
                };
                station.SetValue(Grid.ColumnProperty, i+1);
                stopLabels.Children.Add(station);
            }*/
            //efficiency.Content = Math.Round((1 - decimal.Divide(currentTrain.Unseated, ac.Passengers.Count))*100, 2) + " % Efficiency";
        }
    }

    public class Seat : Grid
    {
        public Online_Offline_Scheduling.Models.Seat seat;
        public int Size;

        public Seat(Online_Offline_Scheduling.Models.Seat s, int size = 10)
        {
            seat = s;
            Size = size;
            InitializeSeat();
        }

        public void Reload()
        {
            
        }

        public void AddPassenger()
        {
            
        }

        private void InitializeSeat()
        {
            Height = 70;
            Margin = new Thickness(0, 0, 0, 3);
            for (int i = 0; i < Size; i++)
            {
                ColumnDefinitions.Add(new ColumnDefinition());
            }

            var number = new Label
            {
                Content = $"Seat # {seat.Number}",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                FontSize = 14
            };
            Children.Add(number);


            Grid trip = new Grid
            {
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xB9, 0x6E))
            };
            trip.SetValue(ColumnProperty, 1);
            trip.SetValue(ColumnSpanProperty, Size);
            Children.Add(trip);

            foreach (var p in seat.Passengers)
            {
                Border bSeat = new Border
                {
                    Height = 60,
                    Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xEC, 0xF3, 0x48)),
                    BorderBrush = new SolidColorBrush(Colors.Black),
                    BorderThickness = new Thickness(1),
                    Margin = new Thickness(2, 0, 2, 0)
                };
                    bSeat.SetValue(ColumnProperty, p.Start + 1);
                bSeat.SetValue(ColumnSpanProperty, p.Stop - p.Start);

                TextBlock passenger = new TextBlock
                {
                    FontSize = 13.333,
                    TextWrapping = TextWrapping.Wrap,
                    TextAlignment = TextAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Text = $"Passenger #{p.Number} ({p.Start} -> {p.Stop})"
                };
                bSeat.Child = passenger;

                Children.Add(bSeat);
            }

        }
    }
}

