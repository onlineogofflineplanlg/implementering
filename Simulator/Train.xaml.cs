﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Simulator
{
    /// <summary>
    /// Interaction logic for Train.xaml
    /// </summary>
    public partial class Train : Window
    {
        public Train()
        {
            InitializeComponent();
            SeatGrid g = new SeatGrid(29, 20);
            Seats.Children.Add(g);
        }
    }
}
