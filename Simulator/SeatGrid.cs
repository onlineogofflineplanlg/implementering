﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Online_Offline_Scheduling;
using Online_Offline_Scheduling.Models;

namespace Simulator
{
    class SeatGrid : Grid
    {
        public SeatGrid(int size, int endStation)
        {
            Seat = new Online_Offline_Scheduling.Models.Seat(1);
            EndStation = endStation;
            Size = size;

            Seat.Passengers.Add(new Passenger(2, 6));
            InitializeVisuals();
        }

        private Grid _passengerGrid = null;

        private int Size { get; set; }

        private int EndStation { get; set; }

        private Online_Offline_Scheduling.Models.Seat Seat { get; set; }

        public void AddPassenger(Passenger p)
        {
            
        }

        public void UpdatePassengers()
        {
            ResetPassengerGrid();
            for (int i = 0; i < EndStation; i++)
            {
                _passengerGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            foreach (var passenger in Seat.Passengers.OrderBy(x => x.Start))
            {
                Border passengerGrid = new Border
                {
                    Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xEC, 0xF3, 0x48)),
                    BorderBrush = new SolidColorBrush(Colors.Black),
                    BorderThickness = new System.Windows.Thickness(1, 0, 1, 0),
                };
                passengerGrid.SetValue(ColumnProperty, passenger.Start);
                passengerGrid.SetValue(ColumnSpanProperty, passenger.Stop - passenger.Start);

                Border passengerNumber = new Border
                {
                    VerticalAlignment = System.Windows.VerticalAlignment.Center,
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                    Padding = new System.Windows.Thickness(2),
                    CornerRadius = new System.Windows.CornerRadius(2),
                    BorderBrush = new SolidColorBrush(Color.FromArgb(0xFF, 0x2A, 0x32, 0x2A)),
                    Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x3B, 0x4F, 0x3B)),
                    BorderThickness = new System.Windows.Thickness(2)

                };

                TextBlock passengerLabel = new TextBlock
                {
                    Text = passenger.Number.ToString(),
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                    VerticalAlignment = System.Windows.VerticalAlignment.Center,
                    Foreground = new SolidColorBrush(Colors.White),
                    TextAlignment = System.Windows.TextAlignment.Center
                };

                passengerNumber.Child = passengerLabel;
                passengerGrid.Child = passengerNumber;
                _passengerGrid.Children.Add(passengerGrid);
            }
        }

        public void InitializeVisuals()
        {
            Height = 28;
            ColumnDefinitions.Add(new ColumnDefinition { Width = new System.Windows.GridLength(70) });
            ColumnDefinitions.Add(new ColumnDefinition {Width = new System.Windows.GridLength(1)});
            ColumnDefinitions.Add(new ColumnDefinition());

            StackPanel seatText = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Margin = new System.Windows.Thickness(4, 0, 0, 0)
            };

            Border seatTextBorder = new Border
            {
                Padding = new System.Windows.Thickness(6, 2, 6, 2),
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x5b, 0xc0, 0xde)),
                Margin = new System.Windows.Thickness(0, 5, 0, 5),
                CornerRadius = new System.Windows.CornerRadius(3)
            };

            TextBlock seatTextBorderBlock = new TextBlock
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                Foreground = new SolidColorBrush(Colors.White),
                Text = Seat.Number.ToString()
            };
            seatTextBorder.Child = seatTextBorderBlock;
            seatText.Children.Add(seatTextBorder);

            TextBlock seatTextBlock = new TextBlock
            {
                Margin = new System.Windows.Thickness(3, 0, 0, 0),
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                Text = "Seat"
            };
            seatText.Children.Add(seatTextBlock);
            Children.Add(seatText);

            Grid seperator = new Grid
            {
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x8E, 0x8E, 0x8E))
            };
            seperator.SetValue(ColumnProperty, 1);
            Children.Add(seperator);

            _passengerGrid = new Grid
            {
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x5C, 0xB8, 0x5C))
            };
            _passengerGrid.SetValue(ColumnProperty, 2);
            Children.Add(_passengerGrid);

            UpdatePassengers();
        }

        private void ResetPassengerGrid()
        {
            _passengerGrid = new Grid
            {
                Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x5C, 0xB8, 0x5C))
            };
            _passengerGrid.SetValue(ColumnProperty, 2);
            Children.RemoveAt(2);
            Children.Add(_passengerGrid);
        }
    }
}
