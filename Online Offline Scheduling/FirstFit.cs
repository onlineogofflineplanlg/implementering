﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Logic;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling
{
    class OldFirstFit
    {
        public void Run()
        {
            List<string> lines = new List<string>();
            lines.Add("Test;Type;Result");
            List<int> test1 = new List<int>();
            List<int> test2 = new List<int>();
            List<int> test3 = new List<int>();
            List<int> test4 = new List<int>();
            List<int> test5 = new List<int>();
            List<int> bestfitTest1 = new List<int>();
            List<int> bestfitTest2 = new List<int>();
            List<int> bestfitTest3 = new List<int>();
            List<int> bestfitTest4 = new List<int>();
            List<int> bestfitTest5 = new List<int>();
            for (int i = 0; i < 1000; i++)
            {
                List<Passenger> passengers = Generator.GetPassengers(25, 0, 14);
                List<Passenger> sortedPassengers = new List<Passenger>(passengers)
                    .OrderBy(x => x.Stop)
                    .ThenBy(x => x.Start)
                    .ToList();
                List<Passenger> notSortedPassengers = new List<Passenger>(passengers)
                    .OrderBy(x => x.Start)
                    .ThenBy(x => x.Stop)
                    .ToList();
                List<Passenger> sortedPassengers2 = new List<Passenger>(passengers)
                    .OrderBy(x => x.Stop)
                    .ThenByDescending(x => x.Start)
                    .ToList();
                List<Passenger> sortedPassengers3 = new List<Passenger>(passengers)
                    .OrderBy(x => x.Start)
                    .ThenByDescending(x => x.Stop)
                    .ToList();
                int seats = 10;

                Train train = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train.AssignSeats(passengers);
                test1.Add(train.Unseated);
                lines.Add($"First Fit;Unsorted;{train.Unseated}");
                //System.Diagnostics.Debug.WriteLine(train);

                Train train2 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train2.AssignSeats(sortedPassengers);
                test2.Add(train2.Unseated);
                lines.Add($"First Fit;Sorted Stop to Start;{train2.Unseated}");
                //System.Diagnostics.Debug.WriteLine(train2);

                Train train3 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train3.AssignSeats(notSortedPassengers);
                test3.Add(train3.Unseated);
                lines.Add($"First Fit;Sorted Start to Stop;{train3.Unseated}");
                //System.Diagnostics.Debug.WriteLine(train3);

                Train train4 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train4.AssignSeats(sortedPassengers2);
                test4.Add(train4.Unseated);
                lines.Add($"First Fit;Sorted Stop to Start Desc;{train4.Unseated}");

                Train train5 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train5.AssignSeats(sortedPassengers3);
                test5.Add(train5.Unseated);
                lines.Add($"First Fit;Sorted Start to Stop Desc;{train5.Unseated}");

                Train train6 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train6.AssignSeats(passengers, Train.FittingAlgorithm.Best);
                bestfitTest1.Add(train6.Unseated);
                lines.Add($"Best Fit;Unsorted;{train6.Unseated}");

                Train train7 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train7.AssignSeats(sortedPassengers, Train.FittingAlgorithm.Best);
                bestfitTest2.Add(train7.Unseated);
                lines.Add($"Best Fit;Sorted Stop to Start;{train7.Unseated}");

                Train train8 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train8.AssignSeats(notSortedPassengers, Train.FittingAlgorithm.Best);
                bestfitTest3.Add(train8.Unseated);
                lines.Add($"Best Fit;Sorted Start to Stop;{train8.Unseated}");

                Train train9 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train9.AssignSeats(sortedPassengers2, Train.FittingAlgorithm.Best);
                bestfitTest4.Add(train9.Unseated);
                lines.Add($"Best Fit;Sorted Stop to Start Desc;{train9.Unseated}");

                Train train10 = new Train(seats)
                {
                    Start = 0,
                    Stop = 13
                };
                train10.AssignSeats(sortedPassengers3, Train.FittingAlgorithm.Best);
                bestfitTest5.Add(train10.Unseated);
                lines.Add($"Best Fit;Sorted Start to Stop Desc;{train10.Unseated}");
            }
            System.Diagnostics.Debug.WriteLine("--First Fit--");
            System.Diagnostics.Debug.WriteLine(decimal.Divide(test1.Sum(), test1.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(test2.Sum(), test2.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(test3.Sum(), test3.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(test4.Sum(), test4.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(test5.Sum(), test5.Count));
            System.Diagnostics.Debug.WriteLine("--Best Fit--");
            System.Diagnostics.Debug.WriteLine(decimal.Divide(bestfitTest1.Sum(), bestfitTest1.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(bestfitTest2.Sum(), bestfitTest2.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(bestfitTest3.Sum(), bestfitTest3.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(bestfitTest4.Sum(), bestfitTest4.Count));
            System.Diagnostics.Debug.WriteLine(decimal.Divide(bestfitTest5.Sum(), bestfitTest5.Count));

            TextWriter tw = new StreamWriter("lang.csv");
            foreach (var line in lines)
            {
                tw.WriteLine(line);
            }
            tw.Close();
        }


    }
}
