﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling.Logic.Sorting
{
    public class BestFit : SortPassengers
    {

        public override List<Seat> Sort(Type type = Type.None)
        {
            var passengers = new List<Passenger>(GetPassengers(type));
            var noPlace = 0;
            foreach (var passenger in passengers)
            {
                var available = 100;
                Seat selectedSeat = null;
                foreach (var seat in Seats)
                {
                    if (seat.Occupied(passenger)) continue;
                    var counter = 0;
                    for (var i = 0; i < passenger.Start; i++)
                    {
                        if (!seat.Occupied(i, i + 1))
                        {
                            counter++;
                        }
                        else
                        {
                            counter = 0;
                        }
                    }
                    if (counter >= available) continue;
                    available = counter;
                    selectedSeat = seat;
                }
                if (selectedSeat != null)
                    selectedSeat.Passengers.Add(passenger);
                else
                    noPlace++;
            }
            Unseated = noPlace;
            return Seats;
        }
        

        public BestFit(List<Seat> seats) : base(seats)
        {
        }

        public BestFit(List<Passenger> passengers, int seats) : base(passengers, seats)
        {
            
        }
    }
}
