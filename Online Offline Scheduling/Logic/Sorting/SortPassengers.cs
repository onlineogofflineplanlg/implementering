﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling.Logic.Sorting
{
    public abstract class SortPassengers
    {
        protected SortPassengers(List<Seat> seats)
        {
            Seats = seats;
        }

        protected SortPassengers(List<Passenger> passengers, int seats)
        {
            Passengers = passengers;
            var startSeatss = new List<Seat>();
            for (var i = 1; i <= seats; i++)
                startSeatss.Add(new Seat(i));
            Seats = startSeatss;
        }

        protected SortPassengers(List<Passenger> passengers)
        {
            Passengers = passengers;
        }

        public int Unseated;

        protected List<Seat> Seats { get; set; } 

        protected List<Passenger> Passengers = new List<Passenger>();

        public void SetPassengers(List<Passenger> passengers)
        {
            Passengers = new List<Passenger>(passengers);
        }

        public List<Passenger> GetPassengers(Type sortingType = Type.None)
        {
            var passengers = new List<Passenger>(Passengers);
            switch (sortingType)
            {
                case Type.None:
                    return passengers;
                case Type.StopToStart:
                    return passengers.OrderBy(x => x.Stop)
                    .ThenBy(x => x.Start)
                    .ToList();
                case Type.StartToStop:
                    return passengers.OrderBy(x => x.Start)
                    .ThenBy(x => x.Stop)
                    .ToList();
                case Type.StopToDecreasingStart:
                    return passengers.OrderBy(x => x.Stop)
                    .ThenByDescending(x => x.Start)
                    .ToList();
                case Type.StartToDecreasingStop:
                    return passengers.OrderBy(x => x.Start)
                    .ThenByDescending(x => x.Stop)
                    .ToList();
            }
            return null;
        }

        public void SetSeats(List<Seat> seats)
        {
            Seats = seats;
        }
        public abstract List<Seat> Sort(Type type = Type.None);
        protected int GetUnseated()
        {
            return Unseated;
        }

        public enum Type
        {
            None,
            StopToStart,
            StartToStop,
            StopToDecreasingStart,
            StartToDecreasingStop
        }
        
    }
}
