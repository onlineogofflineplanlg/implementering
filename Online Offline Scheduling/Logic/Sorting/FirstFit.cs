﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling.Logic.Sorting
{
    public class FirstFit : SortPassengers
    {
        public FirstFit(List<Seat> seats) : base(seats)
        {
        }

        public FirstFit(List<Passenger> passengers, int seats) : base(passengers, seats)
        {
        }

        public FirstFit(List<Passenger> passengers) : base(passengers)
        {
        }

        public override List<Seat> Sort(Type type = Type.None)
        {
            var passengers = new List<Passenger>(GetPassengers(type));
            int unseated = 0;
            foreach (var passenger in passengers)
            {
                bool seated = false;
                foreach (var seat in Seats)
                {
                    if (seat.Occupied(passenger))
                        continue;
                    seat.Passengers.Add(passenger);
                    seated = true;
                    break;
                }
                if (!seated)
                    unseated++;
            }
            Unseated = unseated;
            return Seats;
        }
    }
}
