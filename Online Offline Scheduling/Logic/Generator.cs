﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling.Logic
{
    public class Generator
    {
        public static List<Passenger> GetPassengers(int count, int start, int end, Interval selectectedInterval = Interval.Mixed)
        {
            List<Passenger> passengers = new List<Passenger>();
            int split = 3;
            int shortReq = selectectedInterval == Interval.Mixed ? count/split : count;
            int mediumReq = selectectedInterval == Interval.Mixed ? count /split : count;
            int largeReq = count;
            List<int> distances = DivisionToList(count, 3);
            for (int i = 0; i < count; i++)
            {
                List<Interval> intervals = new List<Interval>();
                if (distances[0] > 0 && (selectectedInterval == Interval.Mixed || selectectedInterval == Interval.Short))
                    intervals.Add(Interval.Short);
                if (distances[1] > 0 && (selectectedInterval == Interval.Mixed || selectectedInterval == Interval.Medium))
                    intervals.Add(Interval.Medium);
                if (distances[2] > 0 && (selectectedInterval == Interval.Mixed || selectectedInterval == Interval.Large))
                    intervals.Add(Interval.Large);
                Interval selected = intervals[StaticRandom.Rand(0, intervals.Count)];
                if (selectectedInterval == Interval.Mixed)
                {
                    switch (selected)
                    {
                        case Interval.Short:
                            distances[0]--;
                            break;
                        case Interval.Medium:
                            distances[1]--;
                            break;
                        case Interval.Large:
                            distances[2]--;
                            break;
                    }
                }
                KeyValuePair<int, int> interval = GetInterval(end, selected);
                int size = StaticRandom.Rand(interval.Key, interval.Value);
                int place = StaticRandom.Rand(start, end - size);
                passengers.Add(new Passenger(place, place + size) { Number = i, Type = selected });
                /*
                if (shortReq > 0 && (selectectedInterval == Interval.Mixed || selectectedInterval == Interval.Short) )
                    intervals.Add(Interval.Large);
                if (mediumReq > 0 && (selectectedInterval == Interval.Mixed || selectectedInterval == Interval.Medium))
                    intervals.Add(Interval.Medium);
                if (selectectedInterval == Interval.Mixed)
                {
                    if (count / split + count / split < largeReq && (selectectedInterval == Interval.Mixed || selectectedInterval == Interval.Large))
                        intervals.Add(Interval.Short);
                }
                else if(selectectedInterval == Interval.Large)
                    intervals.Add(Interval.Large);
                Interval selected = intervals[StaticRandom.Rand(0, intervals.Count)];
                if (selected == Interval.Short)
                    shortReq--;
                else if (selected == Interval.Medium)
                    mediumReq--;
                else
                    largeReq--;
                KeyValuePair<int, int> interval = GetInterval(end, selected);
                int size = StaticRandom.Rand(interval.Key, interval.Value);
                int place = StaticRandom.Rand(start, end - size);
                passengers.Add(new Passenger(place, place + size) {Number = i, Type = selected});*/


            }
            var fordeling = passengers
                .GroupBy(u => u.Type)
                .Select(grp => grp.ToList())
                .ToList();
            Shuffle(passengers);
            return passengers;
        }

        public static List<Passenger> GetRandomPassengers(int count, int start, int end)
        {
            List<Passenger> passengers = new List<Passenger>();
            for (int i = 1; i <= count; i++)
            {
                int distance = StaticRandom.Rand(1, end);
                int place = StaticRandom.Rand(start, end - distance);
                passengers.Add(new Passenger(place, place + distance) { Number = i});
            }
            return passengers;
        }

        public static class StaticRandom
        {
            static int _seed = Environment.TickCount;

            private static readonly ThreadLocal<Random> Random =
                new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref _seed)));

            public static int Rand(int min, int max)
            {
                return Random.Value.Next(min, max);
            }
        }

        public enum Interval
        {
            Short,
            Medium,
            Large,
            Mixed
        }


        public static KeyValuePair<int, int> GetInterval(int max, Interval interval)
        {

            switch (interval)
            {
                case Interval.Short:
                    return new KeyValuePair<int, int>(2, (int) Math.Floor(decimal.Divide(max, 3)));
                case Interval.Medium:
                    return new KeyValuePair<int, int>((int) Math.Floor(decimal.Divide(max, 3)) + 1,
                        (int) Math.Floor(decimal.Divide(max, 3) + decimal.Divide(max, 3)));
                case Interval.Large:
                    return new KeyValuePair<int, int>((int)Math.Floor(decimal.Divide(max, 3) + decimal.Divide(max, 3)) + 1, max);
                default:
                    throw new ArgumentOutOfRangeException(nameof(interval), interval, null);
            }
        }

        public static void Shuffle<T>(IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private static List<int> DivisionToList(int number, int divideBy)
        {
            List<int> numbers = new List<int>();
            int basis = number/divideBy;
            for (int i = 1; i < divideBy; i++)
            {
                numbers.Add(basis);
                number = number - basis;
            }
            numbers.Add(number);
            return numbers;
        }
    }
}
