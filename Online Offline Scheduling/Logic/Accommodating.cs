﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Logic.Sorting;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling.Logic
{
    public class Accommodating
    {
        public Accommodating(int passengerCount = 75)
        {
            Passengers = Generator.GetRandomPassengers(passengerCount, Train.Start, Train.Stop);
        }

        public int Unseated { get; set; }

        public Train Train { get; set; } = new Train(50, 0, 15);

        public List<Passenger> Passengers { get; set; }

        public void PlacePassengers()
        {
            SortPassengers sort = new BestFit(Passengers, Train.Seats.Count);
            Train.Seats = sort.Sort(SortPassengers.Type.StopToStart);
            Unseated = sort.Unseated;
            System.Diagnostics.Debug.Write(Train);
        }

        public List<Passenger> ExtractPassengers(bool shuffle = false)
        {
            var passengers = Train.Seats.SelectMany(seat => seat.Passengers).ToList();
            if (shuffle)
                Generator.Shuffle(passengers);
            return passengers;
        }

        public decimal CalcWasted()
        {
            int size = 15;
            int max = size*Train.Seats.Count;
            int wasted = 0;
            foreach (var seat in Train.Seats)
            {
                for (int i = 0; i < size-1; i++)
                {
                    if (!seat.Occupied(i, i + 1))
                        wasted++;
                }
            }
            return decimal.Divide(wasted, max)*100;
        }
    }
}
