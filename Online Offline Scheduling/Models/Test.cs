﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Online_Offline_Scheduling.Models
{
    public class Test
    {
        public int Number { get; set; }


        public TestDataType TestData { get; set; }

        public Logic.Sorting.SortPassengers.Type SortingMethod { get; set; }

        public Algorithm Algorithm { get; set; }

        public double Seated { get; set; }

        public int PassengerCount { get; set; }
    }

    public enum TestDataType
    {
        Short,
        Medium,
        Long,
        Mixed,
        Random
    }

    public enum Algorithm
    {
        FirstFit,
        BestFit
    }
}
