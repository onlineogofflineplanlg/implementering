﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling
{
    public class Train
    {
        public Train(int seats)
        {
            ReloadSeats(seats);
        }

        public Train(int seats, int start, int stop)
        {
            ReloadSeats(seats);
            Start = start;
            Stop = stop;
        }

        public void ReloadSeats(int seats)
        {
            SeatsAvailable = seats;
            Seats.Clear();
            _seatIndex = 1;
            for (int i = 0; i < seats; i++)
            {
                AddSeat();
            }
        }

        public int SeatsAvailable { get; set; } = 5;

        public List<Seat> Seats = new List<Seat>();

        public int Unseated; 

        public int Start { get; set; }

        public int Stop { get; set; }

        private int _seatIndex = 1;

        private void AddSeat()
        {
            if (SeatsAvailable +1 == _seatIndex)
                return;
            Seats.Add(new Seat(_seatIndex));
            _seatIndex++;
        }

        public override string ToString()
        {
            string r = "Seats: " + Seats.Count;
            foreach (var seat in Seats)
            {
                r += "\n--Seat #" + seat.Number;
                r = seat.Passengers.OrderBy(x => x.Start).Aggregate(r, (current, passenger) => current + ("\n  --Passenger " + passenger.Start + " - " + passenger.Stop));
            }
            return r;
        }

        public void AssignSeats(List<Passenger> passengers, FittingAlgorithm fa = FittingAlgorithm.First)
        {
            if (fa == FittingAlgorithm.First)
                AssignSeatsFirstFit(passengers);
            else
                AssignSeatsBestFit(passengers);

        }

        public enum FittingAlgorithm
        {
            First,
            Best
        }

        private void AssignSeatsFirstFit(List<Passenger> passengers)
        {
            int noPlace = 0;
            foreach (var passenger in passengers)
            {

                var results =
                    Seats.Where(
                        x =>
                           !(x.Passengers.Exists(y => y.Start <= passenger.Stop) &&
                            x.Passengers.Exists(y => y.Stop > passenger.Start))).ToList();
                if (results.Count == 0)
                    noPlace++;
                else
                    results.First().Passengers.Add(passenger);
            }
            Unseated = noPlace;
            //System.Diagnostics.Debug.WriteLine($"Number of passengers that couldn't be placed: {noPlace}");
        }
        
        private void AssignSeatsBestFit(List<Passenger> passengers)
        {
            int noPlace = 0;
            foreach (var passenger in passengers)
            {
                int available = 100;
                Seat selectedSeat = null;
                foreach (var seat in Seats)
                {
                    if (seat.Occupied(passenger)) continue;
                    var counter = 0;
                    for (var i = 0; i < passenger.Start; i++)
                    {
                        if (!seat.Occupied(i, i + 1))
                        {
                            counter++;
                        }
                        else
                        {
                            counter = 0;
                        }
                    }
                    if (counter >= available) continue;
                    available = counter;
                    selectedSeat = seat;
                }
                if (selectedSeat != null)
                    selectedSeat.Passengers.Add(passenger);
                else
                    noPlace++;
            }
            Unseated = noPlace;
            //System.Diagnostics.Debug.WriteLine($"Number of passengers that couldn't be placed: {noPlace}");
        }
    }
}
