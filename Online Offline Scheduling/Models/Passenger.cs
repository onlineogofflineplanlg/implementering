﻿using Online_Offline_Scheduling.Logic;

namespace Online_Offline_Scheduling.Models
{
    public class Passenger
    {
        public Passenger(int start, int stop)
        {
            Start = start;
            Stop = stop;
        }

        public int Start { get; set; }
        public int Stop { get; set; }
        public int Number { get; set; }

        public int Length => Stop - Start;

        public Generator.Interval Type { get; set; }

    }
}
