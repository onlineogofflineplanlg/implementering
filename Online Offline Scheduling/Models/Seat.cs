﻿using System.Collections.Generic;
using System.Linq;

namespace Online_Offline_Scheduling.Models
{
    public class Seat
    {
        public Seat(int number)
        {
            Number = number;
        }

        public int Number { get; set; }

        public List<Passenger> Passengers = new List<Passenger>();

        public bool Occupied(Passenger o)
        {
            return Occupied(o.Start, o.Stop);
        }

        public bool Occupied(int start, int stop)
        {
            return Passengers.Any(passenger => passenger.Start < stop && passenger.Stop > start);
        }
    }
}
