﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Online_Offline_Scheduling.Logic;
using Online_Offline_Scheduling.Logic.Sorting;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*var manager = new ProjectManager();
            chart1.Init(manager);*/
            Main.Run();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SortPassengers.Type sortingType = SortPassengers.Type.None;
            switch (sortingCb.SelectedIndex)
            {
                case 0:
                    sortingType = SortPassengers.Type.None;
                    break;
                case 1:
                    sortingType = SortPassengers.Type.StopToStart;
                    break;
                case 2:
                    sortingType = SortPassengers.Type.StartToStop;
                    break;
                case 3:
                    sortingType = SortPassengers.Type.StopToDecreasingStart;
                    break;
                case 4:
                    sortingType = SortPassengers.Type.StartToDecreasingStop;
                    break;
            }

            Type sort = null;
            if (algorithm.SelectedIndex == 0)
            {
                sort = typeof(FirstFit);
            }
            else if (algorithm.SelectedIndex == 1)
            {
                sort = typeof(BestFit);
            }
            int pas = Convert.ToInt32(passengers.Value);
            CreateTest(pas, sortingType, sort, testDataCb.SelectedIndex);
        }

        public List<Passenger> GetTestData(int index, int pas)
        {
            switch (index)
            {
                case 0:
                    return Generator.GetPassengers(pas, 0,
                        Convert.ToInt32(stops.Value), Generator.Interval.Short);
                case 1:
                    return Generator.GetPassengers(pas, 0,
                        Convert.ToInt32(stops.Value), Generator.Interval.Medium);
                case 2:
                    return Generator.GetPassengers(pas, 0,
                        Convert.ToInt32(stops.Value), Generator.Interval.Large);
                case 3:
                    return Generator.GetPassengers(pas, 0,
                        Convert.ToInt32(stops.Value));
                case 4:
                    return Generator.GetRandomPassengers(pas, 0,
                        Convert.ToInt32(stops.Value));
            }
            return null;
        }

        public TestDataType GetTestDataType(int index)
        {
            switch (index)
            {
                case 0:
                    return TestDataType.Short;
                case 1:
                    return TestDataType.Medium;
                case 2:
                    return TestDataType.Long;
                case 3:
                    return TestDataType.Mixed;
                case 4:
                    return TestDataType.Random;
            }
            return TestDataType.Short;
        }

        public List<Test> CreateTest(int pas, SortPassengers.Type sortingType, Type sortPassengersAlgorithm, int index, bool createGraph = true)
        {
            string AlgoName = sortPassengersAlgorithm == typeof(FirstFit) ? "FirstFit" : "Best";
            var tdt = GetTestDataType(index);
            string name = tdt + " - " + AlgoName + " - " +
                          sortingType;
            string final = name;
            int sameTest = 1;
            while (_tests.Exists(x => x.Name == final))
            {
                sameTest++;
                final = name + sameTest;
            }
            List<Test> tests = new List<Test>();
            for (int test = 1; test <= Convert.ToInt32(testCount.Value); test++)
            {
                if (accommodating.Checked)
                    pas = 1000;
                Train t = new Train(Convert.ToInt32(seats.Value), 0, Convert.ToInt32(stops.Value));
                List<Passenger> testData = new List<Passenger>(GetTestData(index, pas));

                var groupedCustomerList = testData
                .GroupBy(u => u.Type)
                .Select(grp => grp.ToList())
                .ToList();
                if (accommodating.Checked)
                {
                    SortPassengers ac = new BestFit(testData, t.Seats.Count);
                    t.Seats = ac.Sort(SortPassengers.Type.StopToStart);
                    testData = ExtractPassengers(t, true);
                }
                SortPassengers sort = null;
                if (sortPassengersAlgorithm == typeof(FirstFit))
                {
                    sort = new FirstFit(testData, t.Seats.Count);
                }
                else if (sortPassengersAlgorithm == typeof(BestFit))
                {
                    sort = new BestFit(testData, t.Seats.Count);
                }
                
                t.Seats = sort.Sort(sortingType);
                double seated = testData.Count - sort.Unseated;

                if (accommodating.Checked)
                    seated = Math.Round(seated / testData.Count, 4);

                Test tD = new Test
                {
                    Algorithm = sortPassengersAlgorithm == typeof(FirstFit) ? Algorithm.FirstFit : Algorithm.BestFit,
                    Number = test,
                    Seated = seated,
                    SortingMethod = sortingType,
                    TestData = tdt,
                    PassengerCount = testData.Count
                };
                tests.Add(tD);
            }


            if (!createGraph) return tests;
            _tests.Add(new TestView
            {
                Name = final,
                RawName = name,
                Tests = tests,
                Algorithm = AlgoName,
                DataType = tdt,
                SortingType = sortingType
            });
            checkedListBox1.Items.Add(final);
            checkedListBox1.SetItemChecked(checkedListBox1.Items.Count - 1, true);
            CreateChart();
            button2.Enabled = true;

            return tests;
        }

        readonly List<TestView> _tests = new List<TestView>();

        void CreateChart()
        {
            chart1.Series.Clear();
            foreach (var test in _tests.Where(x => x.Visible))
            {
                Series series = new Series(test.Name);
                DataPoint dp = new DataPoint();
                double avg = Math.Round(test.Tests.Average(x => x.Seated), 4);
                dp.Label = avg.ToString(CultureInfo.InvariantCulture);
                dp.SetValueY(avg);

                series.Points.Add(dp);
                chart1.Series.Add(series);
            }
        }

        public string GetSortingName(string input)
        {
            return input.TakeWhile(ctx => ctx != '(').Aggregate(string.Empty, (current, ctx) => current + ctx);
        }

        public class TestView
        {
            public bool Visible { get; set; } = true;

            public string Name { get; set; }

            public TestDataType DataType { get; set; }

            public SortPassengers.Type SortingType { get; set; }

            public string Algorithm { get; set; }


            public string RawName { get; set; }

            public List<Test> Tests { get; set; }
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
                _itemsChecked++;
            else
                _itemsChecked--;
            button5.Enabled = _itemsChecked >= 2;
            _tests[e.Index].Visible = e.NewValue == CheckState.Checked;
            CreateChart();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            TextWriter tw = new StreamWriter(dt.ToString("HHmmss dd-MM-yy") + ".csv");
            foreach (var testView in _tests.Where(x=>x.Visible))
            {
                foreach (var test in testView.Tests)
                {
                    tw.WriteLine($"{test.TestData};{test.Algorithm};{test.SortingMethod};{test.PassengerCount};{test.Seated}");
                }
            }
            tw.Close();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            _itemsChecked = 0;
            _tests.Clear();
            checkedListBox1.Items.Clear();
            chart1.Series.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            chart1.Height = 500;
            int[] intervals = {150, 125, 100};
            foreach (var interval in intervals)
            {
                passengers.Value = interval;
                for (int i = 0; i < 2; i++)
                {
                    algorithm.SelectedIndex = i;
                    for (int j = 0; j < 5; j++) //test
                    {
                        sortingCb.SelectedIndex = j;
                        for (int k = 0; k < 5; k++)
                        {
                            testDataCb.SelectedIndex = k;
                            button1.PerformClick();
                        }
                        //save
                        string name = algorithm.SelectedItem + " - " + interval + " - " + sortingCb.SelectedItem.ToString().Replace(" -> ", "");
                        chart1.SaveImage("Tests//" + name + ".png", ChartImageFormat.Png);
                        SaveToCsv("Tests//"+name);
                        button3.PerformClick();
                    }
                }
            }

        }

        private int _itemsChecked = 0;

        void SaveToCsv(string name = null)
        {
            DateTime dt = DateTime.Now;
            TextWriter tw = new StreamWriter((name ?? dt.ToString("HHmmss dd-MM-yy")) + ".csv");
            tw.WriteLine("Test Data;Algoritme;Sortering;Antal Passagerer Testet;Antal Passagerer Placeret");
            foreach (var testView in _tests.Where(x => x.Visible))
            {
                foreach (var test in testView.Tests)
                {
                    tw.WriteLine($"{test.TestData};{test.Algorithm};{test.SortingMethod};{test.PassengerCount};{test.Seated}");
                }
            }
            tw.Close();
        }

        public List<Passenger> ExtractPassengers(Train train, bool shuffle = false)
        {
            var passengers = train.Seats.SelectMany(seat => seat.Passengers).ToList();
            if (shuffle)
                Generator.Shuffle(passengers);
            return passengers;
        }

        public decimal CalcWasted(Train train)
        {
            int size = 15;
            int max = size * train.Seats.Count;
            int wasted = 0;
            foreach (var seat in train.Seats)
            {
                for (int i = 0; i < size - 1; i++)
                {
                    if (!seat.Occupied(i, i + 1))
                        wasted++;
                }
            }
            return decimal.Divide(wasted, max) * 100;
        }

        private void accommodating_CheckedChanged(object sender, EventArgs e)
        {
            passengers.Enabled = !accommodating.Checked;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CompareTests(_tests);
        }

        public double CompareTests(List<TestView> tests)
        {
            int start = Convert.ToInt32(seats.Value);
            TestView test1 = tests[0];
            TestView test2 = tests[1];
            TextWriter tw = new StreamWriter($"{test1.Name} against {test2.Name} m.csv");
            //Dictionary<int, double> results = new Dictionary<int, double>();
            string line = "Passenger Tested";
            foreach (var testView in tests)
            {
                line += ";" + testView.Name;
            }
            tw.WriteLine(line);
            while (true)
            {
                line = start.ToString();
                foreach (var testView in tests)
                {
                    List<Test> testsForTest = CreateTest(start, testView.SortingType, GetSortingTypeFromString(testView.Algorithm),
                    GetIndexFromDataType(testView.DataType), false);
                    var average = testsForTest.Average(x => x.Seated);
                    textBox1.AppendText($"[{start}] {average} - {testView.Name}\n");
                    line += ";" + average;
                }
                tw.WriteLine(line);
                /*List<Test> testsForTest1 = CreateTest(start, test1.SortingType, GetSortingTypeFromString(test1.Algorithm),
                    GetIndexFromDataType(test1.DataType), false);
                List<Test> testsForTest2 = CreateTest(start, test2.SortingType, GetSortingTypeFromString(test2.Algorithm),
                    GetIndexFromDataType(test2.DataType), false);
                var test1Avg = testsForTest1.Average(x => x.Seated);
                var test2Avg = testsForTest2.Average(x => x.Seated);
                var difference = test2Avg/test1Avg;
                if (min == 0 && difference != 1.0)
                    min = start;
                lastDifference = difference;
                if (difference > lastDifference)
                    max = start;
                results.Add(start, difference);*/
                //tw.WriteLine($"{start};{test1Avg};{test2Avg};{Math.Round(difference, 4)}");
                //textBox1.AppendText($"Difference for {start} passengers: {difference}\n");
                start += 50;
                if (start > 3000)
                    break;
            }
            textBox1.AppendText("Done!");
            tw.Close();
            return 2.1;
        }

        public int GetIndexFromDataType(TestDataType tdt)
        {
            switch (tdt)
            {
                case TestDataType.Short:
                    return 0;
                case TestDataType.Medium:
                    return 1;
                case TestDataType.Long:
                    return 2;
                case TestDataType.Mixed:
                    return 3;
                case TestDataType.Random:
                    return 4;
            }
            return 0;
        }

        public Type GetSortingTypeFromString(string type)
        {
            if (type == "FirstFit")
                return typeof(FirstFit);
            return typeof(BestFit);
        }
    }
}
