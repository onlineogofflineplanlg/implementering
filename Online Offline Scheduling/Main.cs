﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Offline_Scheduling.Logic;
using Online_Offline_Scheduling.Logic.Sorting;
using Online_Offline_Scheduling.Models;

namespace Online_Offline_Scheduling
{
    public static class Main
    {
        public static void Run()
        {
            Accommodating ac = new Accommodating(110);
            ac.PlacePassengers();
            //List<Seat> accomodating = AccomodatingSeats(25, Train.FittingAlgorithm.Best);
        }

        public static List<Seat> AccomodatingSeats(int passengers, Train.FittingAlgorithm fittingAlgorithm)
        {
            List<Passenger> passengerList = Generator.GetPassengers(passengers, 0, 14)
                .OrderBy(x => x.Stop)
                .ThenByDescending(x => x.Start)
                .ToList();
            Train train = new Train(passengers);
            train.AssignSeats(passengerList, fittingAlgorithm);
            return train.Seats.Where(x => x.Passengers.Count > 0).ToList();

        }
    }
}
